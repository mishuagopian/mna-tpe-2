
### Compressing wav file
function [compression, distortion] = compression (filename, epsilon, L)

    L = 2^L;

    [wavFile, wavFileSize] = getWavFile(filename);

    wavFFT = fft(wavFile);

    [wavFFT, originalLength] = reduce(wavFFT, epsilon);

    wavFFT = compress(wavFFT, L);

    compression = calculateCompression(wavFileSize, wavFFT, L);

    wavFFT = expand(wavFFT, originalLength);

    compressedWavFile = ifft(wavFFT);

    saveWavFile(compressedWavFile, filename);

    distortion = calculateDistortion(wavFile, compressedWavFile);

endfunction


### Compressing wavFFT values
function wavFFT = compress (wavFFT, L)

    mappingCompressionValues = calculateCompressionMappingValues(wavFFT, L);

    realWavFFT = real(wavFFT);
    imaginaryWavFFT = imag(wavFFT);

    realMappingCompressionValues = mappingCompressionValues(:,1);
    imaginaryMappingCompressionValues = mappingCompressionValues(:,2);

    wavFFTLength = length(wavFFT);

    for j = 1:wavFFTLength
        realWavFFT(j) = mapToCompressedValue(realMappingCompressionValues, realWavFFT(j));
        imaginaryWavFFT(j) = mapToCompressedValue(imaginaryMappingCompressionValues, imaginaryWavFFT(j));
    end

    wavFFT = realWavFFT + imaginaryWavFFT * i;

endfunction


### Mapping value to the closest posible value in compressedPosibleValues
function value = mapToCompressedValue (compressedPosibleValues, value)

    compressedPosibleValuesLength = length(compressedPosibleValues);

    if (value > compressedPosibleValues(compressedPosibleValuesLength))
        value = compressedPosibleValues(compressedPosibleValuesLength);
    else
        for i = 0:(compressedPosibleValuesLength-1)
            if (value <= compressedPosibleValues(compressedPosibleValuesLength - i))
                aux = compressedPosibleValues(compressedPosibleValuesLength - i);
            else
                value = aux;
                break;
            end
        end
    end

endfunction


### Returning mappingCompressionValues matrix that contains values to map
### wavFFT values in order to compress in log2(L) bits
function mappingCompressionValues = calculateCompressionMappingValues (wavFFT, L)

    realWavFFT = real(wavFFT);
    imaginaryWavFFT = imag(wavFFT);

    minReal = min(realWavFFT);
    maxReal = max(realWavFFT);
    minImaginary = min(imaginaryWavFFT);
    maxImaginary = max(imaginaryWavFFT);

    realStep = (maxReal - minReal) / L;
    imaginaryStep = (maxImaginary - minImaginary) / L;

    mappingCompressionValues = [transpose(minReal:realStep:maxReal) transpose(minImaginary:imaginaryStep:maxImaginary)];

endfunction


### Returning half + 1 coefficients and truncating to 0 when smaller than epsilon
function [wavFFT, originalLength] = reduce (wavFFT, epsilon)

    originalLength = length(wavFFT);
    newLength = ceil(originalLength / 2) + 1;
    wavFFT = wavFFT(1:newLength);

    for j = 1:newLength
        if (abs(wavFFT(j)) < epsilon)
            wavFFT(j) = 0;
        end
    end

endfunction


### Re-adding previously deleted coefficients, but from new mapped coefficients in wavFFT
function wavFFT = expand (wavFFT, originalLength)

    wavFFTLength = length(wavFFT);

    for j = 1:(wavFFTLength-1)
        wavFFT(2 * (wavFFTLength - 1) - j) = conj(wavFFT(j+1));
    end	

endfunction


### Returning wavFile from filename without extension
function [wavFile, wavFileSize] = getWavFile (filename)

    filename = [filename ".wav"];

    wavFile = wavread(filename);

    [info, err, msg] = lstat(filename);
    wavFileSize = info.size * 8;

endfunction


### Creates a wavFile with the given filename (without extension)
function saveWavFile(wavFile, filename)

    filename = [filename "Processed.wav"];
    wavwrite(wavFile, filename);

endfunction


### Calculates compression distortion
function distortion = calculateDistortion (originalWavFile, compressedWavFile)

    originalWavFile = originalWavFile(1:length(compressedWavFile));
    distortion = (real(originalWavFile) - real(compressedWavFile)).^2;
    distortion = sum(distortion) / length(distortion);

endfunction

### Calculates compression percentage
function compression = calculateCompression(wavFileSize, wavFFT, L)

    realWavFFT = real(wavFFT);
    imaginaryWavFFT = imag(wavFFT);
    c = [realWavFFT; imaginaryWavFFT];

    symbols = unique(c);
    symbolsOccurrences = hist(c, symbols);
    symbolsFreq = symbolsOccurrences / sum(symbolsOccurrences);

    codifSymbols = huffman(symbolsFreq);

    compressedLength = 0;

    compressedLength = L * length(symbols);
    for j = 1:length(symbols)
        compressedLength = compressedLength + length(codifSymbols(j));
    end

    # le tengo que sumar la codificacion del m ınimo y el maximo + 2 puntos flotantes
    compressedLength = compressedLength + 2 * 64;

    # le tengo que sumar la codificacion de la longitud original de x + 1 entero de 32 bits
    compressedLength = compressedLength + 32;

    # le tengo que sumar la codificacion de L + 1 entero de 8 bits
    compressedLength = compressedLength + 8;

    # le tengo que sumar el archivo comprimido
    for j = 1:length(symbols)
        compressedLength = compressedLength + symbolsOccurrences(j) * length(codifSymbols(j));
    end

    compression = compressedLength / wavFileSize;

endfunction

